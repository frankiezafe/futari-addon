/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

#include "futari_modifier.h"

FutariModifier::FutariModifier() :
enabled(FUTARI_MODIFIER_ENABLED),
range(10),
strength(1),
velocity_mult(1),
position_mult(1),
futari_layers(1) {
    _data.ptr = this;
}

FutariModifier::~FutariModifier() {
}

void FutariModifier::set_enabled(bool b) {
#ifdef TOOLS_ENABLED
    if (enabled != b) {
        update_gizmo();
    }
#endif
    enabled = b;
}

bool FutariModifier::get_enabled() const {
    return enabled;
}

void FutariModifier::set_range(real_t r) {
    range = r;
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

real_t FutariModifier::get_range() const {
    return range;
}

void FutariModifier::set_strength(real_t r) {
    strength = r;
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

real_t FutariModifier::get_strength() const {
    return strength;
}

void FutariModifier::set_strength_decay_texture(const Ref<GradientTexture> &tex) {
    strength_decay_texture = tex;
}

Ref<GradientTexture> FutariModifier::get_strength_decay_texture() const {
    return strength_decay_texture;
}

void FutariModifier::set_lifetime_texture(const Ref<GradientTexture> &tex) {
    lifetime_texture = tex;
}

Ref<GradientTexture> FutariModifier::get_lifetime_texture() const {
    return lifetime_texture;
}

void FutariModifier::set_velocity_mult(real_t r) {
    if (r < 0) {
        r = 0;
    } else if (r > 1) {
        r = 1;
    }
    velocity_mult = r;
}

real_t FutariModifier::get_velocity_mult() const {
    return velocity_mult;
}

void FutariModifier::set_position_mult(real_t r) {
    if (r < 0) {
        r = 0;
    } else if (r > 1) {
        r = 1;
    }
    position_mult = r;
}

real_t FutariModifier::get_position_mult() const {
    return position_mult;
}

void FutariModifier::_notification(int p_notification) {

    // MainLoop::
    //NOTIFICATION_WM_MOUSE_ENTER = 2,
    //NOTIFICATION_WM_MOUSE_EXIT = 3,
    //NOTIFICATION_WM_FOCUS_IN = 4,
    //NOTIFICATION_WM_FOCUS_OUT = 5,
    //NOTIFICATION_WM_QUIT_REQUEST = 6,
    //NOTIFICATION_WM_GO_BACK_REQUEST = 7,
    //NOTIFICATION_WM_UNFOCUS_REQUEST = 8,
    //NOTIFICATION_OS_MEMORY_WARNING = 9,
    //NOTIFICATION_TRANSLATION_CHANGED = 90,
    //NOTIFICATION_WM_ABOUT = 91,
    //NOTIFICATION_CRASH = 92,
    //NOTIFICATION_OS_IME_UPDATE = 93,

    // Node::
    //NOTIFICATION_ENTER_TREE = 10,
    //NOTIFICATION_EXIT_TREE = 11,
    //NOTIFICATION_MOVED_IN_PARENT = 12,
    //NOTIFICATION_READY = 13,
    //NOTIFICATION_PAUSED = 14,
    //NOTIFICATION_UNPAUSED = 15,
    //NOTIFICATION_PHYSICS_PROCESS = 16,
    //NOTIFICATION_PROCESS = 17,
    //NOTIFICATION_PARENTED = 18,
    //NOTIFICATION_UNPARENTED = 19,
    //NOTIFICATION_INSTANCED = 20,
    //NOTIFICATION_DRAG_BEGIN = 21,
    //NOTIFICATION_DRAG_END = 22,
    //NOTIFICATION_PATH_CHANGED = 23,
    //NOTIFICATION_TRANSLATION_CHANGED = 24,
    //NOTIFICATION_INTERNAL_PROCESS = 25,
    //NOTIFICATION_INTERNAL_PHYSICS_PROCESS = 26,
    //NOTIFICATION_POST_ENTER_TREE = 27,

    // CanvasItem::
    //NOTIFICATION_TRANSFORM_CHANGED = SceneTree::NOTIFICATION_TRANSFORM_CHANGED, //unique
    //NOTIFICATION_DRAW = 30,
    //NOTIFICATION_VISIBILITY_CHANGED = 31,
    //NOTIFICATION_ENTER_CANVAS = 32,
    //NOTIFICATION_EXIT_CANVAS = 33,
    //NOTIFICATION_LOCAL_TRANSFORM_CHANGED = 35,
    //NOTIFICATION_WORLD_2D_CHANGED = 36,

    // Spatial
    //NOTIFICATION_TRANSFORM_CHANGED = SceneTree::NOTIFICATION_TRANSFORM_CHANGED,
    //NOTIFICATION_ENTER_WORLD = 41,
    //NOTIFICATION_EXIT_WORLD = 42,
    //NOTIFICATION_VISIBILITY_CHANGED = 43,
    //NOTIFICATION_LOCAL_TRANSFORM_CHANGED = 44,

    switch (p_notification) {

        case Node::NOTIFICATION_INTERNAL_PROCESS:
            FutariServer::lock();
            refresh();
            FutariServer::unlock();
            break;

        case Spatial::NOTIFICATION_ENTER_WORLD:
            set_process_internal(true);
            FutariServer::append(data_ptr());
            break;

        case NOTIFICATION_UNPARENTED:
        case NOTIFICATION_EXIT_TREE:
        case NOTIFICATION_EXIT_WORLD:
            FutariServer::remove(data_ptr());
            break;

    }

};

void FutariModifier::set_futari_layer(uint32_t p_mask) {
    futari_layers = p_mask;
}

uint32_t FutariModifier::get_futari_layer() const {
    return futari_layers;
}

void FutariModifier::set_futari_layer_bit(int p_layer, bool p_enable) {
    ERR_FAIL_INDEX(p_layer, 32);
    if (p_enable) {
        set_futari_layer(futari_layers | (1 << p_layer));
    } else {
        set_futari_layer(futari_layers & (~(1 << p_layer)));
    }
}

bool FutariModifier::get_futari_layer_bit(int p_layer) const {
    ERR_FAIL_INDEX_V(p_layer, 32, false);
    return (futari_layers & (1 << p_layer));
}

void FutariModifier::_bind_methods() {

    ClassDB::bind_method(D_METHOD("set_futari_layer", "futari_mask"), &FutariModifier::set_futari_layer);
    ClassDB::bind_method(D_METHOD("get_futari_layer"), &FutariModifier::get_futari_layer);
    ClassDB::bind_method(D_METHOD("set_futari_layer_bit", "futari_layer", "enabled"), &FutariModifier::set_futari_layer_bit);
    ClassDB::bind_method(D_METHOD("get_futari_layer_bit", "futari_layer"), &FutariModifier::get_futari_layer_bit);
    ADD_PROPERTY(PropertyInfo(Variant::INT, "futari_layers", PROPERTY_HINT_LAYERS_3D_RENDER), "set_futari_layer", "get_futari_layer");

    ClassDB::bind_method(D_METHOD("set_enabled", "enabled"), &FutariModifier::set_enabled);
    ClassDB::bind_method(D_METHOD("get_enabled"), &FutariModifier::get_enabled);

    ClassDB::bind_method(D_METHOD("set_range", "range"), &FutariModifier::set_range);
    ClassDB::bind_method(D_METHOD("get_range"), &FutariModifier::get_range);

    ClassDB::bind_method(D_METHOD("set_strength", "strength"), &FutariModifier::set_strength);
    ClassDB::bind_method(D_METHOD("get_strength"), &FutariModifier::get_strength);

    ClassDB::bind_method(D_METHOD("set_strength_decay_texture", "strength_decay"), &FutariModifier::set_strength_decay_texture);
    ClassDB::bind_method(D_METHOD("get_strength_decay_texture"), &FutariModifier::get_strength_decay_texture);

    ClassDB::bind_method(D_METHOD("set_lifetime_texture", "lifetime"), &FutariModifier::set_lifetime_texture);
    ClassDB::bind_method(D_METHOD("get_lifetime_texture"), &FutariModifier::get_lifetime_texture);

    ClassDB::bind_method(D_METHOD("set_velocity_mult", "velocity_mult"), &FutariModifier::set_velocity_mult);
    ClassDB::bind_method(D_METHOD("get_velocity_mult"), &FutariModifier::get_velocity_mult);

    ClassDB::bind_method(D_METHOD("set_position_mult", "position_mult"), &FutariModifier::set_position_mult);
    ClassDB::bind_method(D_METHOD("get_position_mult"), &FutariModifier::get_position_mult);

}