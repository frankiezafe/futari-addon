# source: https://gamedev.stackexchange.com/questions/23625/how-do-you-generate-tileable-perlin-noise

import random
import math
import datetime
from PIL import Image

BW = 0
RGB = 1
RGBA = 2

mode = BW

frac = 32
size, freq, octs, data = 128, 1.0/frac, 6, []

perm = range(size*2)
random.shuffle(perm)
perm += perm
dirs = [(math.cos(a * 2.0 * math.pi / size*2),
		 math.sin(a * 2.0 * math.pi / size*2))
		 for a in range(size*2)]

def noise(x, y, per):
	def surflet(gridX, gridY):
		distX, distY = abs(x-gridX), abs(y-gridY)
		polyX = 1 - 6*distX**5 + 15*distX**4 - 10*distX**3
		polyY = 1 - 6*distY**5 + 15*distY**4 - 10*distY**3
		hashed = perm[perm[int(gridX)%per] + int(gridY)%per]
		grad = (x-gridX)*dirs[hashed][0] + (y-gridY)*dirs[hashed][1]
		return polyX * polyY * grad
	intX, intY = int(x), int(y)
	return (surflet(intX+0, intY+0) + surflet(intX+1, intY+0) +
			surflet(intX+0, intY+1) + surflet(intX+1, intY+1))

def fBm(x, y, per, octs):
	val = 0
	for o in range(octs):
		val += 0.5**o * noise(x*2**o, y*2**o, per*2**o)
	return val

for y in range(size):
	for x in range(size):
		d = fBm(x*freq, y*freq, int(size*freq), octs)
		data.append(d)

im = Image.new("L", (size, size))
im.putdata(data, size, size)

path = "noise_s"+str(size)+"_freq"+str(frac)+"_octs"+str(octs)+"_{:%Y%m%d%H%M%S}.png"
im.save(path.format(datetime.datetime.now()))