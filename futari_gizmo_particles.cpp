/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_gizmo_particles.cpp
 * Author: frankiezafe
 * 
 * Created on January 19, 2019, 11:04 AM
 */

#include "futari_gizmo_particles.h"

FutariParticlesGizmoPlugin::FutariParticlesGizmoPlugin() {
    Color gizmo_color = EDITOR_DEF(FUTARI_PARTICLES_COLOR_NAME, Color(FUTARI_PARTICLES_COLOR));
    create_material(FUTARI_PARTICLES_MATERIAL, gizmo_color);
    gizmo_color.a = 0.1;
    create_material(FUTARI_PARTICLES_SOLID_MATERIAL, gizmo_color);
    create_handle_material(FUTARI_HANDLE_MATERIAL);
}

bool FutariParticlesGizmoPlugin::has_gizmo(Spatial *p_spatial) {
    return Object::cast_to<FutariParticles>(p_spatial) != NULL;
}

String FutariParticlesGizmoPlugin::get_name() const {
    return "FutariParticles";
}

bool FutariParticlesGizmoPlugin::is_selectable_when_hidden() const {
    return true;
}

String FutariParticlesGizmoPlugin::get_handle_name(const EditorSpatialGizmo *p_gizmo, int p_idx) const {

    switch (p_idx) {
        case 0: return "Size X";
        case 1: return "Size Y";
        case 2: return "Size Z";
        case 3: return "Pos X";
        case 4: return "Pos Y";
        case 5: return "Pos Z";
    }

    return "";
}

Variant FutariParticlesGizmoPlugin::get_handle_value(EditorSpatialGizmo *p_gizmo, int p_idx) const {

    FutariParticles *particles = Object::cast_to<FutariParticles>(p_gizmo->get_spatial_node());
    return particles->get_visibility_aabb();
}

void FutariParticlesGizmoPlugin::set_handle(EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point) {

    FutariParticles *particles = Object::cast_to<FutariParticles>(p_gizmo->get_spatial_node());

    Transform gt = particles->get_global_transform();
    //gt.orthonormalize();
    Transform gi = gt.affine_inverse();

    bool move = p_idx >= 3;
    p_idx = p_idx % 3;

    AABB aabb = particles->get_visibility_aabb();
    Vector3 ray_from = p_camera->project_ray_origin(p_point);
    Vector3 ray_dir = p_camera->project_ray_normal(p_point);

    Vector3 sg[2] = {gi.xform(ray_from), gi.xform(ray_from + ray_dir * 4096)};

    Vector3 ofs = aabb.position + aabb.size * 0.5;

    Vector3 axis;
    axis[p_idx] = 1.0;

    if (move) {

        Vector3 ra, rb;
        Geometry::get_closest_points_between_segments(ofs - axis * 4096, ofs + axis * 4096, sg[0], sg[1], ra, rb);

        float d = ra[p_idx];

        aabb.position[p_idx] = d - 1.0 - aabb.size[p_idx] * 0.5;
        particles->set_visibility_aabb(aabb);

    } else {
        Vector3 ra, rb;
        Geometry::get_closest_points_between_segments(ofs, ofs + axis * 4096, sg[0], sg[1], ra, rb);

        float d = ra[p_idx] - ofs[p_idx];
        if (d < 0.001)
            d = 0.001;
        //resize
        aabb.position[p_idx] = (aabb.position[p_idx] + aabb.size[p_idx] * 0.5) - d;
        aabb.size[p_idx] = d * 2;
        particles->set_visibility_aabb(aabb);
    }
}

void FutariParticlesGizmoPlugin::commit_handle(EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel) {

    FutariParticles *particles = Object::cast_to<FutariParticles>(p_gizmo->get_spatial_node());

    if (p_cancel) {
        particles->set_visibility_aabb(p_restore);
        return;
    }

    UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
    ur->create_action(TTR("Change Particles AABB"));
    ur->add_do_method(particles, "set_custom_aabb", particles->get_visibility_aabb());
    ur->add_undo_method(particles, "set_custom_aabb", p_restore);
    ur->commit_action();
}

void FutariParticlesGizmoPlugin::redraw(EditorSpatialGizmo *p_gizmo) {

    FutariParticles *particles = Object::cast_to<FutariParticles>(p_gizmo->get_spatial_node());

    p_gizmo->clear();

    Vector<Vector3> lines;
    AABB aabb = particles->get_visibility_aabb();

    for (int i = 0; i < 12; i++) {
        Vector3 a, b;
        aabb.get_edge(i, a, b);
        lines.push_back(a);
        lines.push_back(b);
    }

    Vector<Vector3> handles;

    for (int i = 0; i < 3; i++) {

        Vector3 ax;
        ax[i] = aabb.position[i] + aabb.size[i];
        ax[(i + 1) % 3] = aabb.position[(i + 1) % 3] + aabb.size[(i + 1) % 3] * 0.5;
        ax[(i + 2) % 3] = aabb.position[(i + 2) % 3] + aabb.size[(i + 2) % 3] * 0.5;
        handles.push_back(ax);
    }

    Vector3 center = aabb.position + aabb.size * 0.5;
    for (int i = 0; i < 3; i++) {
        Vector3 ax;
        ax[i] = 1.0;
        handles.push_back(center + ax);
        lines.push_back(center);
        lines.push_back(center + ax);
    }

    if (p_gizmo->is_selected()) {
        Ref<Material> solid_material = get_material(FUTARI_PARTICLES_SOLID_MATERIAL, p_gizmo);
        p_gizmo->add_solid_box(solid_material, aabb.get_size(), aabb.get_position() + aabb.get_size() / 2.0);

        Ref<Material> rmat = particles->get_process_material();
        if (rmat.is_valid()) {
            Material* m = *rmat;
            FutariMaterial* fm = Object::cast_to<FutariMaterial>(m);
            if (fm) {

                FutariMaterial::FEmissionShape fmes = fm->get_emission_shape();

                switch (fmes) {
                    case FutariMaterial::FUEM_SHAPE_SPHERE:
                    {
                        Vector<Vector3> circle_dots;
                        FutariModifierGizmoPlugin::circle(fm->get_emission_sphere_radius(), circle_dots);
                        for (int r = 0; r < 3; ++r) {
                            Basis matrix;
                            switch (r) {
                                case 1:
                                    matrix.rotate(Vector3(1, 0, 0), M_PI / 2);
                                    break;
                                case 2:
                                    matrix.rotate(Vector3(0, 1, 0), M_PI / 2);
                                    break;
                            }
                            for (int i = 0, j = 1; i < FUTARI_CIRCLE_DEFINITION; ++i, ++j) {
                                j %= FUTARI_CIRCLE_DEFINITION;
                                lines.push_back(matrix.xform(circle_dots[i]));
                                lines.push_back(matrix.xform(circle_dots[j]));
                            }
                        }
                    }
                        break;
                    case FutariMaterial::FUEM_SHAPE_CYLINDER:
                    {
                        Vector<Vector3> circle_dots;
                        Vector3 elevation( 0, fm->get_emission_cylinder_height(), 0 );
                        FutariModifierGizmoPlugin::circle(fm->get_emission_cylinder_radius(), circle_dots);
                        Basis matrix;
                        matrix.rotate(Vector3(1, 0, 0), M_PI / 2);
                        for (int i = 0, j = 1; i < FUTARI_CIRCLE_DEFINITION; ++i, ++j) {
                            j %= FUTARI_CIRCLE_DEFINITION;
                            lines.push_back(matrix.xform(circle_dots[i]) + elevation);
                            lines.push_back(matrix.xform(circle_dots[j]) + elevation);
                        }
                        for (int i = 0, j = 1; i < FUTARI_CIRCLE_DEFINITION; ++i, ++j) {
                            j %= FUTARI_CIRCLE_DEFINITION;
                            lines.push_back(matrix.xform(circle_dots[i]) - elevation);
                            lines.push_back(matrix.xform(circle_dots[j]) - elevation);
                        }

                    }
                        break;
                    case FutariMaterial::FUEM_SHAPE_BOX:
                    {
                        Vector3 bb = fm->get_emission_box_extents();

                        lines.push_back(Vector3(bb.x, bb.y, bb.z));
                        lines.push_back(Vector3(bb.x, bb.y, -bb.z));
                        lines.push_back(Vector3(bb.x, bb.y, -bb.z));
                        lines.push_back(Vector3(-bb.x, bb.y, -bb.z));
                        lines.push_back(Vector3(-bb.x, bb.y, -bb.z));
                        lines.push_back(Vector3(-bb.x, bb.y, bb.z));
                        lines.push_back(Vector3(-bb.x, bb.y, bb.z));
                        lines.push_back(Vector3(bb.x, bb.y, bb.z));

                        lines.push_back(Vector3(bb.x, bb.y, bb.z));
                        lines.push_back(Vector3(bb.x, -bb.y, bb.z));

                        lines.push_back(Vector3(bb.x, bb.y, -bb.z));
                        lines.push_back(Vector3(bb.x, -bb.y, -bb.z));

                        lines.push_back(Vector3(-bb.x, bb.y, -bb.z));
                        lines.push_back(Vector3(-bb.x, -bb.y, -bb.z));

                        lines.push_back(Vector3(-bb.x, bb.y, bb.z));
                        lines.push_back(Vector3(-bb.x, -bb.y, bb.z));

                        lines.push_back(Vector3(bb.x, -bb.y, bb.z));
                        lines.push_back(Vector3(bb.x, -bb.y, -bb.z));
                        lines.push_back(Vector3(bb.x, -bb.y, -bb.z));
                        lines.push_back(Vector3(-bb.x, -bb.y, -bb.z));
                        lines.push_back(Vector3(-bb.x, -bb.y, -bb.z));
                        lines.push_back(Vector3(-bb.x, -bb.y, bb.z));
                        lines.push_back(Vector3(-bb.x, -bb.y, bb.z));
                        lines.push_back(Vector3(bb.x, -bb.y, bb.z));

                    }
                        break;
                    default:
                        break;
                }

            }
        }

    }

    Ref<Material> material = get_material(FUTARI_PARTICLES_MATERIAL, p_gizmo);

    p_gizmo->add_lines(lines, material);

    p_gizmo->add_handles(handles, get_material(FUTARI_HANDLE_MATERIAL));

}