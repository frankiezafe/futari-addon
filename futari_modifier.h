/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_modifier.h
 * Author: frankiezafe
 *
 * Created on January 14, 2019, 9:28 PM
 */

#ifndef FUTARI_MODIFIER_H
#define FUTARI_MODIFIER_H

#include <iostream>

#include "core/rid.h"
#include "scene/3d/spatial.h"

#include "futari_server.h"

#define FUTARI_MODIFIER_ENABLED true

class FutariModifier;

/* Exchange class to retrieve informations and changed flags
 * from any futari modifier present in the scene.
 * Particles manager holds these objects.
 * This class is the equivalent of a struct.
 */
class FutariModifierData {
public:

    enum FutariModifierType {
        FMT_NONE = -1,
        FMT_WIND = 0,
        FMT_ATTRACTOR = 1,
        FMT_VORTEX = 2,
        FMT_FLOOR = 3,
        FMT_MAX = 4
    };

    // set in modifier constructor
    const FutariModifier* ptr;
    int futari_type;
    uint32_t futari_layers;

    bool enabled;

    Vector3 position;
    Vector3 orientation;
    real_t strength;
    real_t range;
    real_t velocity_mult;
    real_t position_mult;

    // textures
    Ref<GradientTexture> strength_decay;
    Ref<GradientTexture> lifetime;

    bool changed;
    bool changed_layer;
    bool changed_enabled;
    bool changed_position;
    bool changed_orientation;
    bool changed_strength;
    bool changed_strength_decay;
    bool changed_lifetime;
    bool changed_range;
    bool changed_velocity_mult;
    bool changed_position_mult;

    FutariModifierData() :
    ptr(0), futari_type(FMT_NONE),
    futari_layers(1),
    enabled(FUTARI_MODIFIER_ENABLED),
    strength(0),
    range(0),
    velocity_mult(0),
    position_mult(0),
    changed(false),
    changed_layer(false),
    changed_enabled(false),
    changed_position(false),
    changed_orientation(false),
    changed_strength(false),
    changed_strength_decay(false),
    changed_lifetime(false),
    changed_range(false),
    changed_velocity_mult(false),
    changed_position_mult(false) {

    }

    void reset() {

        changed = false;
        changed_layer = false;
        changed_enabled = false;
        changed_position = false;
        changed_orientation = false;
        changed_strength = false;
        changed_strength_decay = false;
        changed_lifetime = false;
        changed_range = false;
        changed_velocity_mult = false;
        changed_position_mult = false;

    }

    void operator=(const FutariModifierData& src) {

        ptr = src.ptr;
        futari_type = src.futari_type;
        futari_layers = src.futari_layers;

        position = src.position;
        strength = src.strength;
        range = src.range;
        velocity_mult = src.velocity_mult;
        position_mult = src.position_mult;

        strength_decay = src.strength_decay;
        lifetime = src.lifetime;

        changed = src.changed;
        changed_layer = src.changed_layer;
        changed_enabled = src.changed_enabled;
        changed_position = src.changed_position;
        changed_orientation = src.changed_orientation;
        changed_strength = src.changed_strength;
        changed_strength_decay = src.changed_strength_decay;
        changed_lifetime = src.changed_lifetime;
        changed_range = src.changed_range;

        changed_velocity_mult = src.changed_velocity_mult;
        changed_position_mult = src.changed_position_mult;

    }

};

/* Basis of futari modifiers, interacting with futari particles.
 * Mother classes of FutariWind, FutariAttractor & FutariVortex
 */
class FutariModifier : public Spatial {
    GDCLASS(FutariModifier, Spatial)

public:

    FutariModifier();
    virtual ~FutariModifier();

    void set_enabled(bool b);
    bool get_enabled() const;

    void set_range(real_t r);
    real_t get_range() const;

    void set_strength(real_t r);
    real_t get_strength() const;

    void set_strength_decay_texture(const Ref<GradientTexture> &tex);
    Ref<GradientTexture> get_strength_decay_texture() const;

    void set_lifetime_texture(const Ref<GradientTexture> &tex);
    Ref<GradientTexture> get_lifetime_texture() const;

    void set_velocity_mult(real_t r);
    real_t get_velocity_mult() const;

    void set_position_mult(real_t r);
    real_t get_position_mult() const;

    virtual FutariModifierData* data_ptr() = 0;

    void set_futari_layer(uint32_t p_mask);
    uint32_t get_futari_layer() const;

    void set_futari_layer_bit(int p_layer, bool p_enable);
    bool get_futari_layer_bit(int p_layer) const;

protected:


    static void _bind_methods();
    void _notification(int p_notification);

    virtual void refresh() = 0;

    FutariModifierData _data;

    bool enabled;
    real_t range;
    real_t strength;
    real_t velocity_mult;
    real_t position_mult;

    Ref<GradientTexture> strength_decay_texture;
    Ref<GradientTexture> lifetime_texture;

    uint32_t futari_layers;

};

#endif /* FUTARI_MODIFIER_H */

